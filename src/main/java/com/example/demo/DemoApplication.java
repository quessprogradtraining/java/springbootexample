package com.example.demo;

import com.example.demo.components.Info;
import com.example.demo.components.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.swing.*;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {

		//A container gets created by IOC foe object creations when ConfigurableAppleication contx ref is created
		/*ConfigurableApplicationContext context= SpringApplication.run(DemoApplication.class, args);
		Student studentref=context.getBean(Student.class);
		studentref.display();*/
		//A Context Get created and also bean object get created inside it by the name whatever given by the id in xml
		ApplicationContext context=new ClassPathXmlApplicationContext("info.xml");
		Info infoObj=context.getBean("infoBeanObj",Info.class);
		infoObj.display();


	}

}

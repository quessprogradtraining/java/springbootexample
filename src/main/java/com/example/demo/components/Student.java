package com.example.demo.components;

import org.springframework.stereotype.Component;

@Component
public class Student {
    private  String sname;
    private  int standard;
    private int division;

    public Student() {
    }

    public Student(String sname, int standard, int division) {
        this.sname = sname;
        this.standard = standard;
        this.division = division;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public int getStandard() {
        return standard;
    }

    public void setStandard(int standard) {
        this.standard = standard;
    }

    public int getDivision() {
        return division;
    }

    public void setDivision(int division) {
        this.division = division;
    }
    public void display(){
        System.out.println("hi bean");
    }

}


package com.example.demo.components;

public class Info {
   private  String name;
   private  String city;

    public Info() {
    }

    public Info(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public void display(){
        System.out.println(name+" "+city);
    }
}
